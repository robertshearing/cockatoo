var BackgroundObject = {};



(function() {

  console.log("BackgroundObject");

  var colors = [

    {top:[255, 255, 255], bottom:[200, 200, 200]},
    {top:[255, 20, 20], bottom:[210, 100, 100]},
    {top:[232, 228, 163], bottom:[235, 224, 55]},
    {top:[255, 255, 255], bottom:[230, 230, 230]},
    {top:[20, 20, 20], bottom:[100, 100, 100]},
    {top:[255, 255, 255], bottom:[200, 200, 200]},
    {top:[255, 186, 27], bottom:[235, 106, 55]},
    {top:[255, 255, 255], bottom:[250, 250, 250]},
    {top:[255, 255, 255], bottom:[250, 250, 250]},

  ]
  var currentColor = {top:[255, 255, 255], bottom:[255, 255, 255]};
  var targetColor = colors[0];

  var intervalID;

  step = 1/20;
  stepCount = 0;



  BackgroundObject.updateGradient = function(index){
      console.log("update gradient: " + index);
      targetColor = colors[index];
      stepCount = 0;
      intervalID = setInterval(BackgroundObject.animateBackground,10);
  }
  BackgroundObject.animateBackground = function(){
    
    if ( $===undefined ) return;
      stepCount ++;

      var a = step * stepCount;
      var b = (1 - a);

      var r1 = Math.round(a * targetColor.top[0] + b * currentColor.top[0]);
      var g1 = Math.round(a * targetColor.top[1] + b * currentColor.top[1]);
      var b1 = Math.round(a * targetColor.top[2] + b * currentColor.top[2]);
      var color1 = "rgb("+r1+","+g1+","+b1+")";

      var r2 = Math.round(a * targetColor.bottom[0] + b * currentColor.bottom[0]);
      var g2 = Math.round(a * targetColor.bottom[1] + b * currentColor.bottom[1]);
      var b2 = Math.round(a * targetColor.bottom[2] + b * currentColor.bottom[2]);
      var color2 = "rgb("+r2+","+g2+","+b2+")";


      $('body').css({
          background: "-webkit-gradient(linear, left top, left bottom, from("+color1+"), to("+color2+"))"}).css({
          background: "-moz-linear-gradient(top, "+color1+" 0%, "+color2+" 100%)"});

        
        if(stepCount >= 20){
          stepCount = 0;
          currentColor = targetColor;
          clearInterval(intervalID);
        }
  }
  })();

