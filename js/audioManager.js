var AudioObject = {};



(function() {

	var audioPlayer;
	var audioImage;
	var interval;
	var playing = false;

	var tracks = [
		{name:"pablo",	bpm:100, gain:1, power:4.0},
	    {name:"AC-DC",	bpm:145, gain:1.5, power:4.0},
	    {name:"quirky",	bpm:155, gain:1.2, power:3.0},
	    {name:"BuffaloSoldier",	bpm:70, gain:1.3, power:5.0},
		{name:"DeadPrez",	bpm:90, gain:1.1, power:4.0},
		{name:"PrayerInC",	bpm:118, gain:0.8, power:3.0},
		{name:"bit",	bpm:190, gain:2.5, power:10.0},
		{name:"holiday",	bpm:102, gain:1.5, power:4.0},
		{name:"jazz",	bpm:53, gain:0.8, power:4.0}
	];

	AudioObject.volume = 0;
	AudioObject.bpm = 675;
	AudioObject.force = 1;

	console.log("AudioObject");

	if (window.addEventListener) {
        window.addEventListener('load', AudioObject.init);
    } else if (window.attachEvent) {
        window.attachEvent('load', AudioObject.init);
    }


    AudioObject.init = function (){
        audioPlayer = document.createElement('audio');
        
        console.log("INIT SOUND MANAGER");


        squawkPlayer = document.createElement('audio');
        squawkPlayer.src = "audio/squawk.mp3";
        squawkPlayer.pause();

        for(var i = 0; i< tracks.length; i++){
        	//var btn = document.createElement('button');

        	tracks[i].audioPlayer = document.createElement('audio');
        	//tracks[i].audioPlayer.id = tracks[i].name;
        	tracks[i].audioPlayer.src = "audio/" + tracks[i].name + ".mp3";
        	tracks[i].audioPlayer.preload = "none";
        	//tracks[i].audioPlayer.autobuffer = true;

        	//document.body.appendChild(audioPlayer); 
        	//console.log(document);
        	//console.log(document.body);
        	//console.log(audioPlayer.id );

        }

        playing = false;
        paused = false;
    }

    AudioObject.randomTrack = function(){
    	var track = Math.floor(Math.random()*tracks.length);
        AudioObject.changeTrack(track);
    }

    AudioObject.changeTrack = function (index){
    	AudioObject.loadIndex(index);
    	BackgroundObject.updateGradient(index);
    }


    AudioObject.squawk = function(){
		squawkPlayer.play();
    }

    AudioObject.loadIndex = function(index){
    	if(audioPlayer){
    		audioPlayer.pause();
    	}
    	if(playing){
    		audioPlayer.pause();
    		//audioPlayer.stop();
    	}

    	AudioObject.bpm = 67500/tracks[index].bpm;
    	AudioObject.gain = tracks[index].gain;
    	AudioObject.force = tracks[index].power;
    	audioImage = new Image();
		audioImage.src = "audio/" + tracks[index].name + ".jpg";


        audioPlayer = tracks[index].audioPlayer
        audioPlayer.play();

        playing = false;

        if(interval != null) clearInterval(interval);
        interval = setInterval(AudioObject.tick, 200);
    }

    AudioObject.load = function(soundURL, dataImageURL, bpm, gain, power){
    	if(playing){
    		audioPlayer.pause();
    		//audioPlayer.stop();
    	}
    	AudioObject.bpm = 67500/bpm;
    	AudioObject.gain = gain;
    	AudioObject.force = power;
    	audioImage = new Image();
		audioImage.src = dataImageURL;
		audioPlayer.src = soundURL;
        audioPlayer.autobuffer = true;



        audioPlayer.play();

        playing = false;

        if(interval != null) clearInterval(interval);
        interval = setInterval(AudioObject.tick, 200);
    }

	AudioObject.pause = function(){

		paused = true;
		audioPlayer.pause()
	}
	AudioObject.resume = function(){
		paused = false;
		audioPlayer.play();
	}


    AudioObject.tick = function(){
    	if (!playing && audioImage.complete && audioPlayer.readyState >= 4) {
			playing = true;
			volumeData = new VolumeData(audioImage);
			volumeData.gain = AudioObject.gain;
			audioPlayer.play();
		}
		if (!playing || paused) { 
			AudioObject.volume = 0;
			return; 
		}

		
		var t = audioPlayer.currentTime;
        var avgVol = volumeData.getAverageVolume(t-0.1,t);

        AudioObject.volume = avgVol.left;

    }

    AudioObject.init();

})();
