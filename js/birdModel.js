var scene, renderer, camera, container, animation;
var hasMorph = false;
var prevTime = Date.now();
var clock = new THREE.Clock();
var mesh;
var geometryData = {};
var origin = {};

var ready = false;

var canvasTexture;
var canvasMaterial;
var textureContext;

var WIDTH;
var HEIGHT;
var gl;// = renderer.context;
var rtTexture;// = new THREE.WebGLRenderTarget(WIDTH, HEIGHT, {minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat});
var pixel = new Uint8Array(4);
var framebuffer;

var dragging = false;
var mousePosition = {};
var mouseOrigin = {};




var EYE_OPEN = 1;
var EYE_CLOSED = 2;
var EYE_ANGRY = 3;
var EYE_ANGRY_LEFT = 4;
var EYE_ANGRY_RIGHT = 5;

var BEAK_CLOSED = 6;
var BEAK_OPEN = 7;

var FEET_FLAT = 8;
var FEET_TAP = 9;

var currentEye = EYE_OPEN;
var currentMouth = BEAK_CLOSED;
var currentFeet = FEET_FLAT;

var textureImages = [
    {name:'textures/cockatoo.png', loaded:false},
    {name:'textures/eye.png', loaded:false},
    {name:'textures/eye_closed.png', loaded:false},
    {name:'textures/eyes_angry.png', loaded:false},
    {name:'textures/eyes_angry_right.png', loaded:false},
    {name:'textures/eyes_angry_left.png', loaded:false},
    {name:'textures/beak_closed.png', loaded:false},
    {name:'textures/beak_open.png', loaded:false},

    {name:'textures/feet.png', loaded:false},
    {name:'textures/feet_tap.png', loaded:false}
];


function render() {
    mesh.rotation.z = 0.015*window.bounceRate;
    renderer.render( scene, camera );

var time = new Date();
    var timestamp = time.getTime();


    if(dragging){

        var dx = mousePosition.x - mouseOrigin.x;
        mouseOrigin.x += dx*0.25;

        var dy = mousePosition.y - mouseOrigin.y;
        mouseOrigin.y += dy*0.25;

        Demo.updateConstraint(dx*0.2 , dy*0.2);
        updateMouth(BEAK_CLOSED);
        if(dx< -0.1){
            updateEye(EYE_ANGRY_RIGHT);
        }else if (dx > 0.1){
            updateEye(EYE_ANGRY_LEFT);
        }else{
            updateEye(EYE_ANGRY);
        }

        var mouthPhase = Math.sin(timestamp*0.01);
        if(mouthPhase < 0){
            updateMouth(BEAK_CLOSED);
        }else{
            updateMouth(BEAK_OPEN);
        }

    }else{

    
    var phase = Math.sin(timestamp*0.001);
    var tapPhase = Math.sin(timestamp*(6.5/AudioObject.bpm));
    var mouthPhase = Math.sin(timestamp*0.002);
    /*
    if(phase < -0.75){
        updateEye(EYE_OPEN);
    }else{
        updateEye(EYE_CLOSED);
    }
    if(mouthPhase < 0){
        updateMouth(BEAK_CLOSED);
    }else{
        updateMouth(BEAK_OPEN);
    }

*/      
    if(AudioObject.volume < 0.25){
        updateEye(EYE_OPEN);
    }else{
        updateEye(EYE_CLOSED);
    }

    if(AudioObject.volume < 0.8){
        updateMouth(BEAK_CLOSED);
    }else{
        updateMouth(BEAK_OPEN);
    }
    

    if(tapPhase < 0){
        updateFeet(FEET_FLAT);
    }else{
        updateFeet(FEET_TAP);
    }

    }

    //console.log(camera.position.x, camera.position.y, camera.position.z, camera.rotation.x, camera.rotation.y, camera.rotation.z);
}



function onWindowResize() {
    WIDTH = container.offsetWidth;
    HEIGHT = container.offsetHeight;
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
    renderer.setSize( container.offsetWidth, container.offsetHeight );
    rtTexture = new THREE.WebGLRenderTarget(WIDTH, HEIGHT, {minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat});
    render();
}

function setupScene( result, data ) {
    scene = new THREE.Scene();
}

function setupLights() {
    var lightColour = 0xffffff;
    var light = new THREE.AmbientLight(lightColour);
    scene.add( light );
}


function preloadTextures(){
    console.log("preload images")
    var loadingComplete = true;
    for(var i  = 0; i< textureImages.length; i++){
        if(textureImages[i].loaded == false){
            textureImages[i].loaded = true; // well not quite yet!
            var imageElement = document.createElement('img');
            textureImages[i].img = imageElement;
            imageElement.onload = function(e) {
                imageElement.onload = null;
                console.log("image loaded")
                preloadTextures();
            };
            imageElement.src = textureImages[i].name;
            return
        }
    }
    console.log("images fiished loading")
    setupMaterials();
}


function setupMaterials(){
    var canvasMat = document.createElement('canvas');
    textureContext= canvasMat.getContext('2d');
    canvasMat.width = 1024;
    canvasMat.height = 1024;
    canvasTexture = new THREE.Texture(canvasMat);
    canvasMaterial = new THREE.MeshBasicMaterial({
      map: canvasTexture,
      skinning: true,
      transparent: true,
    });

    mesh = new THREE.SkinnedMesh( geometryData.geometry, canvasMaterial, false );
    scene.add( mesh );

    updateTexture();
    ready = true;


    console.log("play random track");
    //AudioObject.randomTrack();
}




function updateEyeMouth(eye, mouth) {
    if(currentEye == eye && currentMouth == mouth) return;
    currentEye = eye;
    currentMouth = mouth;
    updateTexture();
}

function updateMouth(mouth){
    if(currentMouth == mouth) return;
    currentMouth = mouth;
    updateTexture();
}
function updateEye(eye){
    if(currentEye == eye) return;
    currentEye = eye;
    updateTexture();
}
function updateFeet(feet){
    if(currentFeet == feet) return;
    currentFeet = feet;
    updateTexture();
}

function updateTexture(){
    if(textureContext){
     textureContext.clearRect(0, 0, 1024, 1024);
    textureContext.drawImage(textureImages[0].img, 0, 0, 1024, 1024);
    textureContext.drawImage(textureImages[currentEye].img, 690, 309, 60, 60)
    textureContext.drawImage(textureImages[currentMouth].img, 740, 320, 100, 110);
    textureContext.drawImage(textureImages[currentFeet].img, 320, 925, 375, 60);
    canvasTexture.needsUpdate = true;
    }
}




function loadData( data, url ) {

    if ( data.metadata.type != 'Geometry' ) {
        console.warn( 'Expecting geometry!!' );
        return;
    }

    var loader = new THREE.JSONLoader();
    var texturePath = loader.extractUrlBase( url );
    geometryData = loader.parse( data, texturePath );

    setupScene();
    setupLights();
    preloadTextures();


    gl = renderer.context;
    rtTexture = new THREE.WebGLRenderTarget(WIDTH, HEIGHT, {minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat});
    

}

function onMouseDown(e){
        // Render to texture
        renderer.render(scene, camera, rtTexture, true);
        mousePosition.x = e.clientX;
        mousePosition.y = e.clientY;
        // Bind frame buffer to inspect
        framebuffer = rtTexture.__webglFramebuffer;
        gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);

        // Inspect pixel at mouse coords, pass results to pixel variable
        gl.viewport(e.clientX, HEIGHT - e.clientY, 1, 1);
        gl.readPixels(e.clientX, HEIGHT - e.clientY, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixel);

        // Unbind frame buffer
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);

        console.log(pixel); // rgba values
        console.log(!!pixel[3]); // True if not transparent

        if(!!pixel[3]){

            AudioObject.squawk();
            AudioObject.pause();

            if(mousePosition.x < WIDTH*0.5){
                Demo.addMouseDrag("tail");
            }else{
                Demo.addMouseDrag("head");
            }
            
            mouseOrigin.x = mousePosition.x;
            mouseOrigin.y = mousePosition.y;
            container.onmousemove = onMouseMove;
            container.onmouseup = onMouseUp
            dragging = true;
        }
}

function onMouseMove(e){
    console.log("Mouse moved: " + e.clientX + " : " + e.clientY);
    mousePosition.x = e.clientX;
    mousePosition.y = e.clientY;
}
function onMouseUp(e){
    console.log("MOUSE UP");
    container.onmousemove = null;
    container.onmouseup = null;
    dragging = false;
    Demo.releaseConstraint();
    AudioObject.resume();
}

function init( url ) {


    //console.log(url);
    container = document.createElement( 'div' );
    container.id = 'viewport';
    document.body.appendChild( container );

    container.onmousedown = onMouseDown;
    WIDTH = container.offsetWidth;
    HEIGHT = container.offsetHeight;

    renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true  } );
    renderer.setSize( container.offsetWidth, container.offsetHeight );
    renderer.setClearColor( 0x000000, 0 );
    container.appendChild( renderer.domElement );
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    
    var aspect = container.offsetWidth / container.offsetHeight;
    camera = new THREE.PerspectiveCamera( 50, aspect, 0.01, 50 );
    //orbit = new THREE.OrbitControls( camera, container );
   // orbit.addEventListener( 'change', render );
    var target = new THREE.Vector3( 0, 1, 0 );
    camera.lookAt( target );
    //orbit.target = target;
    camera.updateProjectionMatrix();

    camera.position.x = 2.5;
    camera.position.y = 1.0;
    camera.position.z = 0.0;

    camera.rotation.x = 1.5;
    camera.rotation.y = 1.5;
    camera.rotation.z = -1.5;

    window.addEventListener( 'resize', onWindowResize, false );

	var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function ( x ) {
        if ( xhr.readyState === xhr.DONE ) {
            if ( xhr.status === 200 || xhr.status === 0  ) {
                loadData( JSON.parse( xhr.responseText ), url );
            } else {
                console.error( 'could not load json ' + xhr.status );
            }
        }
    };
    xhr.open( 'GET', url, true );
    xhr.withCredentials = false;
    xhr.send( null );



}
var frame = 0;
function draw() {
        setTimeout(function() {
        requestAnimationFrame(draw);


        if(ready == false) return;

        var timestamp = new Date();
        var time = timestamp.getTime();

        frame++;
        //Composite.rotate(head, Math.sin(time*0.001)*0.01, {x:370, y:330})
        //console.log("Global: " + window.neck);
            var pos = {x:Math.sin(time*0.001)*0.01, y:Math.cos(time*0.001)*1.0}
            
            if(!origin.initialised ){
                // check if the physics has initialised
                if(window.center.position != undefined){
                   initialiseBoneOrigins();
                }else{
                    return;
                }
            }
            if(mesh != undefined){
            for(var i = 0; i< mesh.skeleton.bones.length; i++){

                var bone = mesh.skeleton.bones[i];
                switch(bone.name){
                   
                    case "BoneLeg2":
                    modifyArmiture(bone, window.center, window.foot, origin.foot, -0.75, -0.5, -0.0008, -0.0002);
                    break;
                     
                    case "BoneTail1":
                    modifyArmiture(bone, window.tail1, window.center, origin.tail1, 0.7, -0.3, -0.0003, -0.0003, true);
                    break;
                    case "BoneTail2":
                    modifyArmiture(bone, window.tail2, window.tail1, origin.tail2, 0.75, -0.4, 0.0003, -0.0003, true);
                    break;

                    case "BoneNeck1":
                    modifyArmiture(bone, window.neck1, window.center, origin.neck1, -0.2, -0.2, -0.0003, -0.0003);
                    break;

                    case "BoneNeck2":
                    modifyArmiture(bone, window.neck2, window.neck1, origin.neck2, -0.25, -0.3, -0.0004, -0.0003);
                    break;

                    case "BoneNeck3":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.neck3, -0.25, -0.4, -0.0003, -0.0003);
                    break;



                    case "BonePlume1":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.plume1, -0.1, -1.1, 0.0001, 0.0001);
                    break;
                    
                    case "BonePlume2":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.plume2, -0.15, -1.0, 0.0001, 0.0001);
                    break;
                    case "BonePlume3":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.plume3, -0.2, -0.9, 0.0001, 0.0001);
                    break;
                    case "BonePlume4":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.plume4, -0.15, -0.7, 0.0001, 0.0001);
                    break;
                    case "BonePlume5":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.plume5, -0.1, -0.6, 0.0001, 0.0001);
                    break;
                    case "BonePlume6":
                    modifyArmiture(bone, window.neck3, window.neck2, origin.plume6, 0.2, -0.15, 0, 0);
                    break;
                }
                

            }
        render();     
       }
        
        
    }, 1000/60);

    }
    draw();


    function modifyArmiture(bone, target, anchor, pOrigin, angleOffset, rInfluence, xInfluence, yInfluence, normaliseAngle){

        if(angleOffset == undefined) angleOffset = 0;
        if(rInfluence == undefined) rInfluence = -0.5;
        if(xInfluence == undefined) xInfluence = 0.008;
        if(yInfluence == undefined) yInfluence = 0.008;

        var angle = getAngle(anchor.position, target.position, false);
        

        if(normaliseAngle){
            if(angle < 0) angle += Math.PI*2;
            if(angle > Math.PI*2) angle -= Math.PI*2;
        }

        var dx = (target.position.x - pOrigin.x)*xInfluence
        var dy = (target.position.y - pOrigin.y)*yInfluence


        bone.rotation.x = angleOffset + angle*rInfluence;

        bone.position.z =  origin[bone.name].z + dx;
        bone.position.y =  origin[bone.name].y + dy;

    }


function getAngle(p1, p2, degrees){// angle in radians
    var angle = Math.atan2(p2.y - p1.y, p2.x - p1.x);
    

    if(degrees) angle *= 180 / Math.PI;
    return angle;
}



function initialiseBoneOrigins(){
    origin.initialised = true;

    origin.foot = {x:window.foot.position.x, y:window.foot.position.y}
    origin.center = {x:window.center.position.x, y:window.center.position.y}
    origin.tail1 = {x:window.tail1.position.x, y:window.tail1.position.y}
    origin.tail2 = {x:window.tail2.position.x, y:window.tail2.position.y}
    origin.neck1 = {x:window.neck1.position.x, y:window.neck1.position.y}
    origin.neck2 = {x:window.neck2.position.x, y:window.neck2.position.y}
    origin.neck3 = {x:window.neck3.position.x, y:window.neck3.position.y}

    origin.plume1 = {x:window.plume1.position.x, y:window.plume1.position.y}
    origin.plume2 = {x:window.plume2.position.x, y:window.plume2.position.y}
    origin.plume3 = {x:window.plume3.position.x, y:window.plume3.position.y}
    origin.plume4 = {x:window.plume4.position.x, y:window.plume4.position.y}
    origin.plume5 = {x:window.plume5.position.x, y:window.plume5.position.y}
    origin.plume6 = {x:window.plume6.position.x, y:window.plume6.position.y}

    for(var i = 0; i< mesh.skeleton.bones.length; i++){
                var bone = mesh.skeleton.bones[i];
                origin[bone.name] = {x: bone.position.x, y:bone.position.y, z:bone.position.z};
    }

}



init("model/cockatooModel.json");




