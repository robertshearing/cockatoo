//Global vars



//armiture references:
window.foot = 0;
window.center = 0;
window.tail1 = 0;
window.tail2 = 0;
window.neck1 = 0;
window.neck2 = 0;
window.neck3 = 0;
window.plume1 = 0;
window.plume2 = 0;
window.plume3 = 0;
window.plume4 = 0;
window.plume5 = 0;
window.plume6 = 0;
window.accessory = 0;

window.bounceRate = 0;
window.manualConstraint = null;


var Demo = {};


(function() {

    // Matter aliases
    var Engine = Matter.Engine,
        World = Matter.World,
        Bodies = Matter.Bodies,
        Body = Matter.Body,
        Composite = Matter.Composite,
        Composites = Matter.Composites,
        Common = Matter.Common,
        Constraint = Matter.Constraint,
        RenderPixi = Matter.RenderPixi,
        Events = Matter.Events,
        Bounds = Matter.Bounds,
        Vector = Matter.Vector,
        Vertices = Matter.Vertices,
        MouseConstraint = Matter.MouseConstraint,
        Mouse = Matter.Mouse,
        Query = Matter.Query;


        plume1 = Matter.Body;
        plume2 = Matter.Body,
        plume3 = Matter.Body,
        plume4 = Matter.Body,
        plume5 = Matter.Body,
        plume6 = Matter.Body,
        accessory = Matter.Body,
        bird = Matter.Composite,
        legs = Matter.Composite;

        head = Matter.Body;
        tail = Matter.Body;
        center = Matter.Body;



    // MatterTools aliases
    if (window.MatterTools) {
        var Gui = MatterTools.Gui,
            Inspector = MatterTools.Inspector;
    }

    Demo = {};


    console.log("Physics");

    var _theWorld,
        _engine,
        _gui,
        _inspector,
        _sceneName,
        _mouseConstraint,
        _sceneEvents = [],
        _useInspector = window.location.hash.indexOf('-inspect') !== -1,
        _isMobile = /(ipad|iphone|ipod|android)/gi.test(navigator.userAgent);

    
    // initialise the demo
    var count = 0;
    var fps = 60;
    var params = {
        timing: 675,
        x: 0,
        y: 0,
    };


    var offset = 0.5;



    var frame = 0;

    Demo.addMouseDrag = function(target){
        if(manualConstraint != null) Demo.releaseConstraint();

        if(target == "head" ){
                manualConstraint = Constraint.create({ 
                    pointA: {x: neck3.position.x, y: neck3.position.y}, 
                    bodyB: neck3, 
                    stiffness: 0.2,
                });
        }else{
            manualConstraint = Constraint.create({ 
                pointA: {x: center.position.x, y: center.position.y}, 
                bodyB: tail2, 
                stiffness: 0.2,
            });
        }

        World.add(_engine.world, [manualConstraint])
    
    }


    Demo.updateConstraint = function(x, y){
        if(manualConstraint){
            manualConstraint.pointA.x += x;
            manualConstraint.pointA.y += y;
        }
    }

    Demo.releaseConstraint = function(){
        World.remove(_engine.world, [manualConstraint])
        manualConstraint = null;
    }


    function draw() {
        setTimeout(function() {
        requestAnimationFrame(draw);

        var time = new Date();
        var timestamp = time.getTime();

        // Animate the gravity based on sine curves based on dynamic x and y values.
   // _engine.world.gravity.y = params.y * Math.sin(200 * time / (Math.PI * params.timing));
   // _engine.world.gravity.x = params.y * Math.sin(350 * time / (Math.PI * params.timing));
    //_engine.world.gravity.x = params.x * Math.sin(200 * time / 2 / (Math.PI * params.timing) - 1 * Math.PI / 1);

    // Slowly animate the x and y values to add variety to the movement
    // x alternates between 0.5 and 1.5 over 17 seconds
    // y alternates between 0.75 and 1.75 over 42 seconds
    //params.x += (0.50 * Math.sin(20 * _engine.timing.timestamp / (Math.PI * 17 * 1000)) + 1.0 - params.x) / 20;
    //params.y += (0.50 * Math.sin(20 * _engine.timing.timestamp / (Math.PI * 42 * 1000)) + 1.25 - params.y) / 20;

        if(_engine){
//
            if(_engine.world && !manualConstraint){


                
                accessory.force = {x:0.0, y:0.00000001};

               // accessory.update();



                //_engine.world.gravity.y = 0.0;

                //_engine.world.gravity.y  = Math.cos(time*0.01) * 2.0;
                //_engine.world.gravity.x  = Math.sin(time*0.008) * 1.75;
                //console.log(_engine.world.gravity.x , _engine.world.gravity.y)

                //_engine.world.gravity.y = 1 * Math.sin(timestamp *0.005);
                params.timing = AudioObject.bpm;

                var vol = AudioObject.volume*AudioObject.volume*AudioObject.force;
                
               
                _engine.world.gravity.y = vol * Math.sin(20 * timestamp / (Math.PI * params.timing));
                _engine.world.gravity.x = vol * Math.sin(20 * timestamp / 2 / (Math.PI * params.timing) - 1 * Math.PI / 1);
                //_engine.world.gravity.y = 1.0*params.y * Math.sin(20 * timestamp / (Math.PI * params.timing));
                //_engine.world.gravity.x = 1.0*params.x * Math.sin(20 * timestamp / 2 / (Math.PI * params.timing) - 1 * Math.PI / 1);

                bounceRate = _engine.world.gravity.y;

                // Slowly animate the x and y values to add variety to the movement
                // x alternates between 0.5 and 1.5 over 17 seconds
                // y alternates between 0.75 and 1.75 over 42 seconds
                params.x += (1.0 * Math.sin(20 * _engine.timing.timestamp / (Math.PI * 17 * 1000)) + 1.0 - params.x) / 20;
                params.y += (1.0* Math.sin(20 * _engine.timing.timestamp / (Math.PI * 42 * 1000)) + 1.25 - params.y) / 20;


            }
        }
        

        frame++;
        //Composite.rotate(head, Math.sin(time*0.001)*0.01, {x:370, y:330})

        
    }, 1000/fps);

    }
   draw();



    Demo.init = function() {
        var container = document.getElementById('canvas-container');

        // some example engine options
        var options = {
            positionIterations: 1,
            velocityIterations: 1,
            enableSleeping: false
        };

        // create a Matter engine
        // NOTE: this is actually Matter.Engine.create(), see the aliases at top of this file
        _engine = Engine.create(container, options);

        // add a mouse controlled constraint
        _mouseConstraint = MouseConstraint.create(_engine);
        World.add(_engine.world, _mouseConstraint);

        // run the engine
        Engine.run(_engine);

        // default scene function name
       //_sceneName = 'softBody';
        _sceneName = 'cockatoo';
        
        // get the scene function name from hash
        if (window.location.hash.length !== 0) 
            _sceneName = window.location.hash.replace('#', '').replace('-inspect', '');

        // set up a scene with bodies
        Demo.reset();
        Demo[_sceneName]();

        //var gui = new dat.GUI();
       //gui.add(tracker, 'offset', 0, 6).step(1.5);


    };

    // call init when the page has loaded fully

    if (window.addEventListener) {
        window.addEventListener('load', Demo.init);
    } else if (window.attachEvent) {
        window.attachEvent('load', Demo.init);
    }


    Demo.cockatoo = function (){
        var bird = Composites.softBody(120, 350, 8, 2, 10, 10, true, 30, {}, {});
        var legs = Composites.softBody(340, 520, 2, 3, 0, 0, true, 30, {}, {stiffness: 0.75});
        var legSupport = Bodies.circle(430, 490, 30);

        var tail = Bodies.circle(156, 626, 40);
        var head = Bodies.circle(620, 160, 60);


        foot = legs.bodies[4];
        center = bird.bodies[11]

        tail1 = bird.bodies[9];
        tail2 = tail;
        neck1 = bird.bodies[5];
        neck2 = bird.bodies[14];
        neck3 = head;

        accessory = Bodies.circle(620, 280, 10);
        accessory.frictionAir = 0.75;
        accessory.groupId = 10;
        //accessory.force = {x:0.0, y:0.1};
        
 

        Composite.rotate(bird, -0.8, {x:370, y:430})


        plume1 = Bodies.circle(610, 0, 10);
        var plumeSpring1 = Constraint.create({ bodyA: head, bodyB: plume1, stiffness: 0.5});
        var plumeSpring11 = Constraint.create({ bodyA: bird.bodies[7], bodyB: plume1, stiffness: 0.25});

        plume2 = Bodies.circle(570, 12, 10);
        var plumeSpring2 = Constraint.create({ bodyA: head, bodyB: plume2, stiffness: 0.5});
        var plumeSpring22 = Constraint.create({ bodyA: bird.bodies[7], bodyB: plume2, stiffness: 0.25});

        plume3 = Bodies.circle(530, 24, 10);
        var plumeSpring3 = Constraint.create({ bodyA: head, bodyB: plume3, stiffness: 0.5});
        var plumeSpring33 = Constraint.create({ bodyA: bird.bodies[7], bodyB: plume3, stiffness: 0.2});

        plume4 = Bodies.circle(500, 34, 10);
        var plumeSpring4 = Constraint.create({ bodyA: head, bodyB: plume4, stiffness: 0.5});
        var plumeSpring44 = Constraint.create({ bodyA: bird.bodies[6], bodyB: plume4, stiffness: 0.2});

        plume5 = Bodies.circle(470, 50, 10);
        var plumeSpring5 = Constraint.create({ bodyA: head, bodyB: plume5, stiffness: 0.5});
        var plumeSpring55 = Constraint.create({ bodyA: bird.bodies[6], bodyB: plume5, stiffness: 0.2});

        plume6 = Bodies.circle(430, 70, 10);
        var plumeSpring6 = Constraint.create({ bodyA: head, bodyB: plume6, stiffness: 0.5});
        var plumeSpring66 = Constraint.create({ bodyA: bird.bodies[6], bodyB: plume6, stiffness: 0.2});

        var mouse = MouseConstraint.create(_engine, {
            constraint: {stiffness: 0.1}
        });

        var ground = Bodies.rectangle(400, 725, 800, 50, { 
                isStatic: true,
            });

        _engine.world.gravity.y = 0.0;

        foot.render.fillStyle ='red';
        center.render.fillStyle ='red';
        tail1.render.fillStyle ='red';
        tail2.render.fillStyle ='red';
        neck1.render.fillStyle ='red';
        neck2.render.fillStyle ='red';
        neck3.render.fillStyle ='red';




        // Attach the legs to the ground
        var createAnchor = function(x, y, bodyB) {
            return Constraint.create({ 
                pointA: {x: x, y: y}, 
                bodyB: bodyB, 
                stiffness: 2.0,
            });
        };
        var anchor1 = createAnchor(370, 670, legs.bodies[legs.bodies.length - 2]);
        var anchor2 = createAnchor(430, 670, legs.bodies[legs.bodies.length - 1]);

       // var anchor3 = createAnchor(170, 570, legs.bodies[legs.bodies.length - 6]);
       // var anchor4 = createAnchor(630, 570, legs.bodies[legs.bodies.length - 5]);

        // Attach the body to the legs
        var createConnect = function(bodyA, bodyB, stiffness) {
            if(stiffness == undefined) stiffness = 0.6;
            return Constraint.create({ 
                bodyA: bodyA, 
                bodyB: bodyB, 
                stiffness: stiffness,
            });
        };
        var connect1 = createConnect(legs.bodies[0], legSupport);
        var connect2 = createConnect(legs.bodies[1], legSupport);
        

        var connect3 = createConnect(bird.bodies[10], legs.bodies[0]);
        var connect4 = createConnect(bird.bodies[11], legs.bodies[0]);
        var connect5 = createConnect(bird.bodies[10], legSupport);
        var connect6 = createConnect(bird.bodies[11], legSupport);
        var connect7 = createConnect(bird.bodies[12], legSupport);
        var connect12 = createConnect(bird.bodies[11], legs.bodies[1]);
        
        var connect8 = createConnect(bird.bodies[0], tail, 0.4);
        var connect9 = createConnect(bird.bodies[8], tail, 0.4);

        var connect10 = createConnect(bird.bodies[7], head, 0.9);
        var connect11 = createConnect(bird.bodies[15], head, 1.5);


        var connect13 = createConnect(accessory, head, 0.75);

        //var connect12 = createConnect(bird.bodies[15], legSupport);

        //var connect5 = createConnect(bird.bodies[4], plume1.bodies[2]);
        //var connect6 = createConnect(bird.bodies[5], plume1.bodies[2]);

        
        World.add(_engine.world, [mouse, legs, legSupport, bird, tail, head,
            accessory, connect13,
            anchor1, anchor2,
            connect1, connect2, connect3,  connect4, connect6, connect7,connect12,
            connect8, connect9, 
            connect10, connect11,
            //connect12,
/*
            plume1, plumeSpring1,plumeSpring11,
            plume2, plumeSpring2,plumeSpring22,
            plume3, plumeSpring3,plumeSpring33,
            plume4, plumeSpring4,plumeSpring44,
            plume5, plumeSpring5,plumeSpring55,
            plume6, plumeSpring6,plumeSpring66,
            */
            ]);

        var offset = 25;
        World.add(_engine.world, [
            //Bodies.rectangle(400, -offset, 800.5 + 2 * offset, 50.5, { isStatic: true }),
            Bodies.rectangle(400, 700 + offset, 800.5 + 2 * offset, 50.5, { isStatic: true }),
           // Bodies.rectangle(800 + offset, 300, 50.5, 600.5 + 2 * offset, { isStatic: true }),
           // Bodies.rectangle(-offset, 300, 50.5, 600.5 + 2 * offset, { isStatic: true })
        ]);
    }


    Demo.reset = function() {
        var _world = _engine.world;
        
        World.clear(_world);
        Engine.clear(_engine);


        // clear scene graph (if defined in controller)
        var renderController = _engine.render.controller;
        if (renderController.clear)
            renderController.clear(_engine.render);


        // clear all scene events
        for (var i = 0; i < _sceneEvents.length; i++)
            Events.off(_engine, _sceneEvents[i]);
        _sceneEvents = [];

        // reset id pool
        Common._nextId = 0;

        // reset mouse offset and scale (only required for Demo.views)
        //Mouse.setScale(_engine.input.mouse, { x: 1, y: 1 });
        //Mouse.setOffset(_engine.input.mouse, { x: 0, y: 0 });

        _engine.enableSleeping = false;
        _engine.world.gravity.y = 1;
        _engine.world.gravity.x = 0;
        _engine.timing.timeScale = 1;

        var offset = 5;

        _mouseConstraint = MouseConstraint.create(_engine);
        World.add(_world, _mouseConstraint);
        
        var renderOptions = _engine.render.options;
        renderOptions.wireframes = false;
        renderOptions.hasBounds = false;
        renderOptions.showDebug = false;
        renderOptions.showBroadphase = false;
        renderOptions.showBounds = false;
        renderOptions.showVelocity = false;
        renderOptions.showCollisions = false;
        renderOptions.showAxes = false;
        renderOptions.showPositions = false;
        renderOptions.showAngleIndicator = false;
        renderOptions.showIds = false;
        renderOptions.showShadows = false;
        renderOptions.background = '#ffffffff';

        if (_isMobile)
            renderOptions.showDebug = true;
    };
    

})();
